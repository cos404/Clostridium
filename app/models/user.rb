class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :trackable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :confirmable, :validatable

  mount_uploader :avatar, AvatarUploader

  validates :email, presence: true
  validates :username,  presence: true, uniqueness: true

  has_many  :posts
  has_many  :comments
  has_many  :images
  has_many  :post_rating
  has_many  :bookmark

  belongs_to  :role
end
