class PostRating < ApplicationRecord

  belongs_to :post
  belongs_to :user
  
  after_save :rating_update

  def rating_update
    rating = PostRating.where(post_id: self.post_id).sum(:value)
    self.post.update_attribute(:rating, rating)
  end
end
