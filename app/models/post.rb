class Post < ApplicationRecord
  require "translit"

  belongs_to  :user
  
  has_many  :images
  has_many  :comments
  has_many  :post_rating
  has_many  :bookmarks

  accepts_nested_attributes_for :images
  validates :title, :body, :user_id, presence: true

  after_create :url_converter

  def url_converter
    if self.url == nil
      self.update_attributes!(url: "#{self.id}-#{Translit.convert(self.title, :english).gsub!(/[^0-9A-Za-z]/, '-')}")
    else
      self.update_attributes!(url: "#{self.id}-#{self.url}")
    end
  end

  def to_param
    "#{self.url}"
  end

end