class Image < ApplicationRecord
  mount_uploader :title, ImageUploader

  belongs_to :post
  belongs_to :user
 
end