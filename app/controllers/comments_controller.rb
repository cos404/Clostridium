class CommentsController < ApplicationController

  before_action :authenticate_user!
  # before_action :find_comment, only: [:show, :edit, :update, :destroy]
  
  def create
    @post = Post.find(params[:post_id])
  
    @comment = @post.comments.new(comment_params)
    @comment.user_id = current_user.id

    if @comment.save
      redirect_to @post, notice: "Your review was succefuly posted."
    end

  end

  def new
    @comment = Comment.new
  end

  private

  def comment_params
    params.require(:comment).permit(:body, :post_id, :user_id)
  end

  def find_comment
    @comment = Comment.find(params[:id])
  end

end