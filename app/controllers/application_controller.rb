class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :find_post, only: [:post_rating_plus, :post_rating_minus, :add_to_bookmarks]

  helper_method :can_add_posts?, :can_edit_posts?, :can_delete_posts?, :can_add_comments?, :can_edit_comments?, :can_delete_comments?, :can_edit_profile?, :can_delete_profile?, :edit_condition_users?

  # # EDIT POST RATING

  def post_rating_plus
    unless @post.post_rating.where(user_id: current_user.id).empty?
      @post.post_rating.where(user_id: current_user.id).update(value: 1)
    else 
      @post.post_rating.create(user_id: current_user.id, value: 1)
    end
  end

  def post_rating_minus
    unless @post.post_rating.where(user_id: current_user.id).empty?
      @post.post_rating.where(user_id: current_user.id).update(value: -1)
    else 
      @post.post_rating.create(user_id: current_user.id, value: -1)
    end
  end 

  def add_to_bookmarks
    if @post.bookmarks.where(user_id: current_user.id).empty?
      @post.bookmarks.create(user_id: current_user.id)
    else
      bookmark = @post.bookmarks.where(user_id: current_user)
      @post.bookmarks.destroy(bookmark)      
    end
  end

  protected

  # # METHODS FOR EDITING\DELETING CONTENTS
  
  # # CAN_ADD_IT?
  # # (IS IT AUTHORIZED?) &&
  # # (ACTIVE MY PROFILE?) &&
  # # (CAN I ADD IT?)

  # # CAN_EDIT_IT?
  # # (IS IT AUTHORIZED?) && 
  # # (ACTIVE MY PROFILE?) && 
  # # (
  # #   (IT IS MY? && CAN I EDIT IT?) || 
  # #   (CAN I EDIT OTHER POSTS\COMMENTS\PROFILE?)
  # # )

  # # CAN_DELETE_IT? 
  # # (IS IT AUTHORIZED?) &&
  # # (ACTIVE MY PROFILE?) &&
  # # (
  # #   (IT IS MY? && CAN I DELETE IT?) || 
  # #   (CAN I EDIT OTHER POSTS\COMMENTS\PROFILE?)
  # # )
    
  def can_add_posts?
    if user_active? && current_user.role.add_posts
      true
    else
      false
    end
  end

  def can_edit_posts?
    if user_active? && ((@post.user_id == current_user.id && current_user.role.edit_posts) || current_user.role.edit_other_posts)
      true
    else
      false
    end
  end

  def can_delete_posts?
    if user_active? && ((@post.user_id == current_user.id && current_user.role.delete_posts) || current_user.role.delete_other_posts)
      true
    else
      false
    end
  end

  def can_add_comments?
    if user_active? && current_user.role.add_comments
      true
    else
      false
    end
  end

  def can_edit_comments?
    if user_active? && ((@comment.user_id == current_user.id && current_user.role.edit_comments) || current_user.role.edit_other_comments)
      true
    else
      false
    end
  end

  def can_delete_comments?
    if user_active? && ((@comment.user_id == current_user.id && current_user.role.delete_comments) || current_user.role.delete_other_comment)
      true
    else
      false
    end
  end

  def can_edit_profile?
    if user_active? && ((@user.id == current_user.id && current_user.role.edit_profile) || current_user.role.edit_other_profiles)
      true
    else
      false
    end
  end

  def can_delete_profile?
    if user_active? && ((@user.id == current_user.id &&current_user.role.delete_profile) || current_user.role.delete_other_profiles)
      true
    else
      false
    end
  end

  def edit_condition_users?
    if user_active? && current_user.role.edit_condition_users
      true
    else
      false
    end
  end


  # # STRONG PARAMETRES FOR DEVISE
  def configure_permitted_parameters
    added_attrs = [:username, :email, :avatar, :password, :password_confirmatioЦЦn, :remember_me]
    devise_parameter_sanitizer.permit :sign_up, keys: added_attrs
    devise_parameter_sanitizer.permit :account_update, keys: added_attrs
  end

  private

  def user_active?
    if current_user && current_user.profile_status == 'active'
      true
    else
      false
    end
  end

  def find_post
    @post = Post.find(params[:post_id])
  end

end