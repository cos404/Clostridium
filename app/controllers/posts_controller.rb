class PostsController < ApplicationController
  before_action :find_post, only: [:show, :edit, :update, :destroy, :increment_view]
  before_action :increment_view, only: [:show]

  def index
    @posts = Post.all
  end

  def new
    @post = Post.new
    @images = Image.where(user_id: current_user.id, post_id: nil)
  end
 
  def create
    @post = current_user.posts.create(post_params)

    if @post.save
      Image.where(user_id: current_user.id, post_id: nil).update_all(post_id: @post.id)

      if params[:post][:image_ids]
        params[:post][:image_ids].split(" ").each do |id|
          Image.find(id).update_attribute(post_id: @post.id)   
        end
      end
      
      flash[:success] = "Post added!"
      redirect_to @post
    else
      flash.now[:error] = "You have error!"
      render "new"
    end
  end

  def edit
    if can_edit_posts?
      true
    else
      flash[:error] = "You don't have permission!"
      redirect_to root_path
    end 
  end

  def update
    @post.update_attributes(post_params)

    if @post.errors.empty?
      redirect_to @post
    else
      render "edit"
    end
  end

  def destroy
    if can_delete_posts?
      @post.destroy
      redirect_to posts_path
     else
      flash[:error] = "You don't have permission!"
      redirect_to root_path
    end       
  end

  private
  def post_params
    params.require(:post).permit(:title, :body, :source, :image)
  end

  def find_post
    @post = Post.find(params[:id])
  end

  def increment_view
    @post.increment!(:views)
  end
end
