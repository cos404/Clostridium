class UsersController < ApplicationController

  before_action :find_user, only: [:show, :edit, :update]

  def index
    @users = User.all
  end

  def edit
    if can_edit_user?
      true
    else
      flash[:error] = "You don't have permission!"
      redirect_to root_path
    end 
  end

  def update
    @user.update_attributes(user_params)

    if @user.errors.empty?
      redirect_to @user
    else
      render "edit"
    end
  end

  private

  def user_params
    params.require(:user).permit(:email, :password, :status, :avatar, :fullname, :about)
  end

  def find_user
    @user = User.find(params[:id])
  end
  
end
