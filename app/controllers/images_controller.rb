class ImagesController < ApplicationController
  before_action :find_image, only: [:destroy]

  def destroy
    @image.destroy
  end

  def create
    @image = Image.create!(title: params[:image][:image], user_id: current_user.id, post_id: params[:image][:post_id])
  end
  
  private
  def image_params
    params.require(:image).permit(:images)
  end
  
  def find_image
    @image = Image.find(params[:id])
  end
end
