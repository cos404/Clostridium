function wrapText(elementID, tag) {
  this.tag = parseInt(tag);
  var openTag, 
  closeTag,
  tags = ['b', 'u', 'i', 'img', 's', 'code', 'quote', 'url', 'youtube', 'gvideo', 'vimeo', 'email', 'left', 'center', 'right', 'br'];
  if(tag == 15) {
    openTag = '[' + tags[tag] + ']'
    closeTag = ''
  }
  else {
    openTag = '[' + tags[tag] + ']'
    closeTag = '[/' + tags[tag] + ']';
  }
  var textArea = $('#' + elementID);
  var len = textArea.val().length;
  var start = textArea[0].selectionStart;
  var end = textArea[0].selectionEnd;
  var selectedText = textArea.val().substring(start, end);
  var replacement = openTag + selectedText + closeTag;
  textArea.val(textArea.val().substring(0, start) + replacement + textArea.val().substring(end, len));
}

$(function() {
  return $("a[data-remote]").on("ajax:success", function(e, data, status, xhr) {
    $("#img_thumb").load(location.href + " #img_thumb>*", "");
    return alert("The article was deleted.");
  });
});

$(function() {
  $('#image_image').fileupload();
  return {
    dataType: "script"
  };
});