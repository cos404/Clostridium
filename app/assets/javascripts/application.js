// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery-fileupload/basic
//= require jquery-fileupload/vendor/tmpl
//= require turbolinks
//= require_tree .
//= require bootstrap-sprockets

$(document).ready(function() {
  $(".plus").click(function() {
    var current_post = $(this)
    $.ajax({
      url: '/posts/' + current_post.attr('data-post-id') + '/rating_plus',
      type: 'POST'
    });
  });
});

$(document).ready(function() {
  $(".minus").click(function() {
    var current_post = $(this)
    $.ajax({
      url: '/posts/' + current_post.attr('data-post-id') + '/rating_minus',
      type: 'POST'
    });
  });
});

$(document).ready(function() {
  $(".bookmark").click(function(){
    var current_post = $(this)
    $.ajax({
      url: '/posts/' + current_post.attr('data-post-id') + '/add_bookmark',
      type: 'POST' 
    });
  });  
});
