Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: "home#index"
  
  resources :posts do
    resources :comments
    post '/rating_plus' => 'application#post_rating_plus'
    post '/rating_minus' => 'application#post_rating_minus'
    post '/add_bookmark' => 'application#add_to_bookmarks'
  end

  resources :images, only: [:create, :destroy, :show]

  resources :users, only: [:show, :edit, :update, :index]

  namespace :admin do
    resources :posts, :users
  end
  
end
