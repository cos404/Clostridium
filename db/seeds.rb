# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


Role.create(title:                  :user,
            add_posts:              true,
            edit_posts:             false,
            delete_posts:           false,
            add_comments:           true,
            edit_comments:          true,
            delete_comments:        false,
            edit_profile:           false,
            delete_profile:         false,
            edit_other_posts:       false,
            delete_other_posts:     false,
            edit_other_comments:    false,
            delete_other_comments:  false,
            edit_other_profiles:    false,
            delete_other_profiles:  false,
            edit_condition_users:   false
)
Role.create(title:                  :moderator,
            add_posts:              true,
            edit_posts:             true,
            delete_posts:           true,
            add_comments:           true,
            edit_comments:          true,
            delete_comments:        true,
            edit_profile:           false,
            delete_profile:         false,
            edit_other_posts:       true,
            delete_other_posts:     true,
            edit_other_comments:    true,
            delete_other_comments:  true,
            edit_other_profiles:    false,
            delete_other_profiles:  false,
            edit_condition_users:   false
)
Role.create(title:                  :administrator,
            add_posts:              true,
            edit_posts:             true,
            delete_posts:           true,
            add_comments:           true,
            edit_comments:          true,
            delete_comments:        true,
            edit_profile:           true,
            delete_profile:         true,
            edit_other_posts:       true,
            delete_other_posts:     true,
            edit_other_comments:    true,
            delete_other_comments:  true,
            edit_other_profiles:    true,
            delete_other_profiles:  true,
            edit_condition_users:   true
)

User.create!(
  email: "root@root.ru", 
  password: "qwerty", 
  password_confirmation: "qwerty",
  username: "Hash", 
  confirmed_at:           "2017-06-30 21:42:22.989289",
  confirmation_sent_at:   "2017-06-30 21:40:47.864614",
  role_id: 3)


User.create!(
  email: "common_user@root.ru", 
  password: "qwerty", 
  password_confirmation: "qwerty",
  username: "COMMON_USER", 
  confirmed_at:           "2017-06-30 21:42:22.989289",
  confirmation_sent_at:   "2017-06-30 21:40:47.864614")

Post.create!(
  title: "Микроплатежи в GTA Online принесли разработчикам $1,09 миллиарда",
  body: "Еcли вы предполагаете, что разработчики из Rockstar уже вовсю работают над новой частью GTA или хотя бы сюжетным дополнением, то вы глубоко заблуждаетесь. Разработчики продолжают добавлять контент в GTA Online, которая приносит им баснословные суммы в виде микроплатежей от игроков. Аналитики фирмы SuperData уверены, что общий доход GTA Online на подобных транзакциях перевалил за $1 млрд.

Такие данные содержатся в закрытом исследовательском отчете фирмы, на который ссылается ресурс Tweaktown. Благодаря микротранзакциям последняя часть GTA (с ее онлайн-режимом) стала самой успешной цифровой консольной игрой всех времен.

Благодаря игрокам, которые активно донатят, покупая внутриигровую валюту, издательство Take-Two Interactive сообщило о росте доходов в последнем финансовом году: чистая прибыль составила $927,1 млн. Всякие DLC и микротранзакции сгенерировали половину этой прибыли.

Разработчики не собираются останавливаться. Они планируют вводить в онлайн-режим еще больше контента, чтобы удерживать игроков в мультиплеере и, соответственно, подталкивать их к микротранзакциям.",
  user_id: 1)

Post.create!(
  title: "В Беларуси начали принимать биткоин",
  body: "Будущее наступило: в Беларуси отныне можно оплатить покупки криптовалютой. Правда, пока только биткоином и только в рамках одной крупной сети по продаже автомобильных шин и сопутствующих товаров и услуг. Проект реализован совместно с блокчейн-лабораторией Consensuslab. Соответствующая информация появилась в группе Facebook, посвященной развитию блокчейна в Беларуси. 

Судя по всему, это первый случай использования биткоина в нашей стране для покупки товаров. Ранее Нацбанк озвучил позицию, согласно которой криптовалюты в Беларуси находятся вне рамок правового поля, то есть не имеют юридической силы при, например, рассмотрении судебных дел.

Торговая сеть прокомментировала ситуацию: «Мы собираемся принимать платеж на сумму, равную товарному чеку либо заказ-наряду. По белорусскому законодательству мы не можем дарить товар, поэтому даем клиенту скидку 99%. Деньгами человек должен заплатить 1%. Остальное — через биткоины. В нашей налогооблагаемой базе будем указывать сумму, соответствующую стопроцентной выручке чека или заказ-наряда».

Это означает, что оплачивается налог с полной суммы покупки, что должно соответствовать закону. Представители компании отметили, что криптовалюты в Беларуси не запрещены, а их статус вне правового поля означает, что риски ложатся исключительно на компанию.",
  user_id: 2)