# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170729125508) do

  create_table "bookmarks", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "post_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "comments", force: :cascade do |t|
    t.text     "body"
    t.integer  "user_id"
    t.integer  "post_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "images", force: :cascade do |t|
    t.string   "title"
    t.integer  "user_id"
    t.integer  "post_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "post_ratings", force: :cascade do |t|
    t.integer  "value"
    t.integer  "user_id"
    t.integer  "post_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "posts", force: :cascade do |t|
    t.string   "title"
    t.string   "url"
    t.string   "visibility",   default: "public"
    t.text     "body"
    t.integer  "views",        default: 0
    t.integer  "user_id"
    t.integer  "rating",       default: 0
    t.boolean  "verification", default: false
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string   "title"
    t.boolean  "add_posts",             default: false
    t.boolean  "edit_posts",            default: false
    t.boolean  "delete_posts",          default: false
    t.boolean  "add_comments",          default: false
    t.boolean  "edit_comments",         default: false
    t.boolean  "delete_comments",       default: false
    t.boolean  "edit_profile",          default: false
    t.boolean  "delete_profile",        default: false
    t.boolean  "edit_other_posts",      default: false
    t.boolean  "delete_other_posts",    default: false
    t.boolean  "edit_other_comments",   default: false
    t.boolean  "delete_other_comments", default: false
    t.boolean  "edit_other_profiles",   default: false
    t.boolean  "delete_other_profiles", default: false
    t.string   "edit_condition_users",  default: "f"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",       null: false
    t.string   "encrypted_password",     default: "",       null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.integer  "role_id",                default: 1
    t.float    "rating",                 default: 0.0
    t.string   "status"
    t.string   "fullname"
    t.string   "username"
    t.string   "avatar"
    t.text     "about"
    t.string   "profile_status",         default: "active"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["username"], name: "index_users_on_username", unique: true
  end

end
