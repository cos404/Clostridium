class CreatePostRatings < ActiveRecord::Migration[5.0]
  def change
    create_table :post_ratings do |t|

       t.integer  :value
       t.integer  :user_id
       t.integer  :post_id

      t.timestamps
    end
  end
end
