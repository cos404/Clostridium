class CreateRoles < ActiveRecord::Migration[5.0]
  def change
    create_table :roles do |t|
      t.string  :title, unique: true

      t.boolean :add_posts, default: false
      t.boolean :edit_posts, default: false
      t.boolean :delete_posts, default: false
      t.boolean :add_comments, default: false
      t.boolean :edit_comments, default: false
      t.boolean :delete_comments, default: false
      t.boolean :edit_profile, default: false
      t.boolean :delete_profile, default: false

      t.boolean :edit_other_posts, default: false
      t.boolean :delete_other_posts, default: false
      t.boolean :edit_other_comments, default: false
      t.boolean :delete_other_comments, default: false
      t.boolean :edit_other_profiles, default: false
      t.boolean :delete_other_profiles, default: false
      t.string  :edit_condition_users, default: false

      t.timestamps
    end
  end
end
