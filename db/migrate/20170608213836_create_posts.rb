class CreatePosts < ActiveRecord::Migration[5.0]
  def change
    create_table :posts do |t|
      t.string      :title
      t.string      :url
      t.string      :visibility, default: "public"
      t.text        :body
      t.integer     :views, default: 0
      t.integer     :user_id
      t.integer     :rating, default: 0
      t.boolean     :verification, default: false
      
      t.timestamps
    end
  end
end