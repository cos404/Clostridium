class CreateImages < ActiveRecord::Migration[5.0]
  def change
    create_table :images do |t|
      t.string  :title
      t.integer :user_id
      t.integer :post_id
      
      t.timestamps
    end
  end
end
