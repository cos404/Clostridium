# Clostridium
## CMS for your website :)

### TO DO:

- users rating
- admin panel
- custom templates
- multilanguage
- authorization through qr code or telegram 
- registration through telegram
- add validates in models
- add tests

### Complete: 

- add posts
- friendly URL for posts
- multiple uploading images for posts
- users registration
- profile
- comments
- uploading avatars
- posts rating
- bookmarks