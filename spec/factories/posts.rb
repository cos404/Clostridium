FactoryGirl.define  do
  factory :post do
    title         'Белорусская компания вошла в список трендовых на сервисе GitHub'
    body          'Компания Akveo поднялась сегодня на вторую строчку в топе «трендовых разработчиков» на авторитетном ресурсе GitHub. Сервис посвящен хостингу IT-проектов и коллективной разработке проектов. Офис Akveo находится в Минске. Две разработки белорусов (Kitten Tricks и React Native UI Kitten) находятся в числе наиболее популярных репозиториев — хранилищ данных.'
    user_id       1
    visibility    'public'
    verification  true

 
  end
  
end







